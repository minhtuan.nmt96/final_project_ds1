# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 02:11:35 2018

@author: Minh Tuấn1
"""

import pandas as pd
import numpy as np
import math

##=====================================================
## Công thức tính toán 
def medium(array1):
    medium = sum(array1)/len(array1)
    return medium 

def var(array,medium): ## Tính phương sai 1 người với giá trị trung bình của người đó
    l = []
    for i in array:
        res = (i-medium)**2
        l.append(res)
    return sum(l)/(len(array)-1)

def pearson(arr1,arr2): ## Tính hệ số tương quan giữa 2 người 
        l1 = []
        medium1 = medium(arr1)
        medium2 = medium(arr2)
        var1 = var(arr1,medium1)
        var2 = var(arr2,medium2)
        for a,b in zip(arr1,arr2):
            v = (a-medium1)*(b-medium2)
            l1.append(v)
        cos = sum(l1)/(len(arr1)-1)
        return cos/math.sqrt(var1*var2)

def user_similar_for(index,array): # Tính pearson giữa 1 người và 1 tập hợp nhiều người
    user_buy = array[index,:]
    l = []
    for i in range(array.shape[0]):
        array_value = pearson(array[i,:],user_buy)
        l.append(array_value)
    return l

def all_similar(array): # Tính pearson của tất cả mọi người với nhau
    l2 = []
    for i in range(array.shape[0]):
        matrix_value = user_similar_for(i,array)
        l2.append(matrix_value)
    return np.array(l2)


def similar(nub1, nub2, array): 
    ##nub1 là số thứ tự users
    ##nub2 là giá trị muốn lấy bao nhiêu ng tương đồng, mặc định là 2
    ##array là mảng để tính tương đồng giữa nub1 và arry
    pearson_array = user_similar_for(nub1, array)
    ## Lấy thứ tự sau khi đã sorted từ cao xuống thấp và bỏ giá trị 1.0 đầu tiên đi
    item = [item[0] for item in sorted(enumerate(pearson_array),key=lambda i:i[1])]
    item = item[::-1]
    nub2 = nub2 + 1
    item1 = item[1:nub2]
    return item1




