# -*- coding: utf-8 -*-
"""

@author: Minh Tuấn1
"""

import pandas as pd
import numpy as np
from nltk import pos_tag, word_tokenize, sent_tokenize
from nltk.corpus import stopwords
import math 
import string
from package_rs2 import *

file = pd.read_csv("C:/Users/admin/desktop/topic.csv")

fl1 = file#.head(50)
fl_des  = fl1['description']

## Bước 2
lib  = ['/b','li','/li','<','>','/ul','(',')','b','br',"''","/7","/8"]
abc = [word_tokenize(row) for row in fl_des]
## Lọc bỏ tag và dấu câu
new_file1 = [[i for i in row if i not in lib and i not in string.punctuation] for row in abc ] ## Dành cho đếm từ

## Bước 3 Lọc bỏ các từ thừa không cần thiết
lib_stop = stopwords.words('english')
new_file2 = [[i for i in row if i not in lib_stop] for row in new_file1]

## Bước 4
merge_list = [i for row in new_file2 for i in row]
set_merge_list = set(merge_list)

## Bước 5 Đếm tần số xuất hiện của từng từ trong từng bài
count_frequency = [[row.count(i) for i in set_merge_list] for row in new_file2]

## Bước 6
zip_file = [dict(zip(set_merge_list, row)) for row in count_frequency]

## Bước 7
def idf(array):
    leng = len(array)
    idf = {}
    list_filter = [[i for i,k in row.items() if k > 0 ] for row in array]
    item = [i for row in list_filter for i in row]
    dic_idf = [{i:item.count(i)} for i in item]
    
    filter_dic = [dict(t) for t in {tuple(d.items()) for d in dic_idf}]
    for row in filter_dic:
        for item,value in row.items():
            idf[item] = math.log((1+leng)/(1+float(value))) +1
    return idf

def tf_idf(tf,idf):
    tf_idf = {}
    for item, value in tf.items():
        tf_idf[item] = value * idf[item]
    return tf_idf

## Bước 8
result_idf = idf(zip_file)

tf = {}
list_tf = []
leng= [len(row) for row in new_file2]

l = []
for row in zip_file:
    for item,value in row.items():
        for a in leng:
            tf[item] = value/float(a)
    vali = tf_idf(tf,result_idf)
    l.append(vali)

## Bước 9
data = pd.DataFrame(l)


## Bước 10
number_data = data.reset_index()
nd = number_data['index']
data_np = np.array(data)
value = all_similar(data_np)

## Bước 11
val = [list(row) for row in value]
dic_user = [dict(zip(nd,row)) for row in val]

sequen = [sorted(row.items(), key= lambda x:x[1]) for row in dic_user]

ut = [[i[0] for i in row] for row in sequen]        
get_paper = [row[-10:] for row in ut]
sort_item_paper  = [sorted(row) for row in get_paper]
sort_all = sorted(sort_item_paper)

final_value = [sort_all[i] for i in range(0,len(sort_all)) if i == 0 or sort_all[i] != sort_all[i-1]]
print(final_value)



